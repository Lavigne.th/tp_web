<?php
use PHPUnit\Framework\TestCase;
use Symfony\Component\Process\Process;
use GuzzleHttp\Client;

abstract class Utils {

    /*
     * var_dump en moins moche
     */
    public static function var_d($data) {
        echo '<pre>' , var_dump($data) , '</pre>';
    }

    /*
     * Renvoie les données associées à un dinausaur a partir de son nom passée en paramètre.
     *
     * Param : String nom du dinosaur
     * Return : objet contenant les données du dinausaure
     */
    public static function loadDinoFromName($name) {
        $url = "https://allosaurus.delahayeyourself.info/api/dinosaurs/$name";
        $dino = Requests::get($url)->body;
        $dino = json_decode($dino);
        return $dino;
    }

    /*
     * Renvoie un tableau de dinausaures contenant les 3 dinosaures les mieux notés. Ces dinauseaures
     * sont choisi de manière aléatoire.
     *
     * Params : aucun
     * Return : array 3 dinausaures les mieux notés
     */
    public static function getTopRated() {
        $dinos = Requests::get('https://allosaurus.delahayeyourself.info/api/dinosaurs/')->body;
        $dinos = json_decode($dinos);

        $random_keys = array_rand($dinos, 3);
        $res[] = $dinos[$random_keys[0]];
        $res[] = $dinos[$random_keys[1]];
        $res[] = $dinos[$random_keys[2]];

        return $res;
    }

    /*
     * Renvoie tout les dinausaures dans un tableau.
     *
     * Params : aucun
     * Return : array  tableau de dinausaures
     */
    public static  function getAllDinos() {
        $dinos = Requests::get('https://allosaurus.delahayeyourself.info/api/dinosaurs/')->body;
        $dinos = json_decode($dinos);
        return $dinos;
    }

}