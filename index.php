<?php
/**
 * Created by PhpStorm.
 * User: Theo
 * Date: 24/01/2019
 * Time: 10:06
 */

require "vendor/autoload.php";

ini_set('display_errors',1);

$loader = new Twig_Loader_Filesystem(dirname(__FILE__) . '/views');
$twigConfig = array(
    'debug' => true,
);

Flight::register('view', 'Twig_Environment', array($loader, $twigConfig), function ($twig) {
    $twig->addExtension(new Twig_Extension_Debug()); // Add the debug extension
});

Flight::route('/', function(){
    $dinos = Utils::getAllDinos();
    Flight::view()->display('listDino.twig', ["dinos" => $dinos]);
});

Flight::route('/dinosaur/@dino', function($dino){
    $dino = Utils::loadDinoFromName($dino);
    $topRateds = Utils::getTopRated();
    Flight::view()->display('dino.twig', ["dino" => $dino, "topRateds" => $topRateds]);
});

Flight::start();