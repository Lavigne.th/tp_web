<?php
require_once 'vendor/autoload.php';

class PagesIntegrationTest extends IntegrationTest{

    public function test_index()
    {
        $response = $this->make_request("GET", "/");
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContains("Velociraptor", $response->getBody()->getContents());
        $this->assertContains("text/html", $response->getHeader('Content-Type')[0]);
    }

    public function test_one_dino()
    {
        $response = $this->make_request("GET", "/dinosaur/velociraptor");
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContains("velociraptor", $response->getBody()->getContents());
        $this->assertContains("text/html", $response->getHeader('Content-Type')[0]);

        $response = $this->make_request("GET", "/dinosaur/dilophosaurus");
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContains("dilophosaurus", $response->getBody()->getContents());
        $this->assertContains("text/html", $response->getHeader('Content-Type')[0]);

        $response = $this->make_request("GET", "/dinosaur/brachiosaurus");
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertContains("brachiosaurus", $response->getBody()->getContents());
        $this->assertContains("text/html", $response->getHeader('Content-Type')[0]);
    }
}