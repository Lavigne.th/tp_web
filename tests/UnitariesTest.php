<?php

require_once 'vendor/autoload.php';

use PHPUnit\Framework\TestCase;

class UnitariesTest extends TestCase {

    public function test_getTopRated(){
        $topRated = Utils::getTopRated();
        $this->assertEquals(3, count($topRated));

        $this->assertEquals(true, property_exists($topRated[0], 'avatar'));
        $this->assertEquals(true, property_exists($topRated[0], 'uri'));
        $this->assertEquals(true, property_exists($topRated[0], 'description'));
        $this->assertEquals(true, property_exists($topRated[0], 'diet'));
        $this->assertEquals(true, property_exists($topRated[0], 'height'));
        $this->assertEquals(true, property_exists($topRated[0], 'weight'));
        $this->assertEquals(true, property_exists($topRated[0], 'length'));
        $this->assertEquals(true, property_exists($topRated[0], 'name'));
        $this->assertEquals(true, property_exists($topRated[0], 'name_meaning'));
        $this->assertEquals(true, property_exists($topRated[0], 'slug'));

        $this->assertEquals(true, property_exists($topRated[1], 'avatar'));
        $this->assertEquals(true, property_exists($topRated[1], 'uri'));
        $this->assertEquals(true, property_exists($topRated[1], 'description'));
        $this->assertEquals(true, property_exists($topRated[1], 'diet'));
        $this->assertEquals(true, property_exists($topRated[1], 'height'));
        $this->assertEquals(true, property_exists($topRated[1], 'weight'));
        $this->assertEquals(true, property_exists($topRated[1], 'length'));
        $this->assertEquals(true, property_exists($topRated[1], 'name'));
        $this->assertEquals(true, property_exists($topRated[1], 'name_meaning'));
        $this->assertEquals(true, property_exists($topRated[1], 'slug'));

        $this->assertEquals(true, property_exists($topRated[2], 'avatar'));
        $this->assertEquals(true, property_exists($topRated[2], 'uri'));
        $this->assertEquals(true, property_exists($topRated[2], 'description'));
        $this->assertEquals(true, property_exists($topRated[2], 'diet'));
        $this->assertEquals(true, property_exists($topRated[2], 'height'));
        $this->assertEquals(true, property_exists($topRated[2], 'weight'));
        $this->assertEquals(true, property_exists($topRated[2], 'length'));
        $this->assertEquals(true, property_exists($topRated[2], 'name'));
        $this->assertEquals(true, property_exists($topRated[2], 'name_meaning'));
        $this->assertEquals(true, property_exists($topRated[2], 'slug'));
    }

    public function test_loadDinoFromName() {
        $dino = Utils::loadDinoFromName("dilophosaurus");
        $this->assertEquals(true, property_exists($dino, 'avatar'));
        $this->assertEquals(true, property_exists($dino, 'uri'));
        $this->assertEquals(true, property_exists($dino, 'description'));
        $this->assertEquals(true, property_exists($dino, 'diet'));
        $this->assertEquals(true, property_exists($dino, 'height'));
        $this->assertEquals(true, property_exists($dino, 'weight'));
        $this->assertEquals(true, property_exists($dino, 'length'));
        $this->assertEquals(true, property_exists($dino, 'name'));
        $this->assertEquals(true, property_exists($dino, 'name_meaning'));
        $this->assertEquals(true, property_exists($dino, 'slug'));
        $this->assertEquals($dino->name, "Dilophosaurus");

        $dino = Utils::loadDinoFromName("velociraptor");
        $this->assertEquals(true, property_exists($dino, 'avatar'));
        $this->assertEquals(true, property_exists($dino, 'uri'));
        $this->assertEquals(true, property_exists($dino, 'description'));
        $this->assertEquals(true, property_exists($dino, 'diet'));
        $this->assertEquals(true, property_exists($dino, 'height'));
        $this->assertEquals(true, property_exists($dino, 'weight'));
        $this->assertEquals(true, property_exists($dino, 'length'));
        $this->assertEquals(true, property_exists($dino, 'name'));
        $this->assertEquals(true, property_exists($dino, 'name_meaning'));
        $this->assertEquals(true, property_exists($dino, 'slug'));
        $this->assertEquals($dino->name, "Velociraptor");
    }

    public function test_getAllDinos() {
        $dinos = Utils::getAllDinos();
        foreach ($dinos as $dino) {
            $this->assertEquals(true, property_exists($dino, 'avatar'));
            $this->assertEquals(true, property_exists($dino, 'uri'));
            $this->assertEquals(true, property_exists($dino, 'description'));
            $this->assertEquals(true, property_exists($dino, 'diet'));
            $this->assertEquals(true, property_exists($dino, 'height'));
            $this->assertEquals(true, property_exists($dino, 'weight'));
            $this->assertEquals(true, property_exists($dino, 'length'));
            $this->assertEquals(true, property_exists($dino, 'name'));
            $this->assertEquals(true, property_exists($dino, 'name_meaning'));
            $this->assertEquals(true, property_exists($dino, 'slug'));
        }
    }
}